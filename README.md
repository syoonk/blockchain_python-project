**Activate the virtual environment**
'''
source blockchain-env/bin/activate
'''

**Install all pacages**
'''
pip3 install -r requirements.txt
'''

**Run the tests**
Make sure to activate the virtual environment
'''
python3 -m pytest backend/tests
'''
